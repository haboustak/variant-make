include make-utils/vmake.mk

WIN_X64_PREFIX 		:= x86_64-w64-mingw32-
LINUX_ARM_PREFIX	:= arm-linux-gnueabi-
LINUX_X64_PREFIX	:=

WIN_ENVS := \
	$(call make-variant,$(WIN_X64_PREFIX),debug) \
	$(call make-variant,$(WIN_X64_PREFIX),release)
ARM_ENVS := \
	$(call make-variant,$(LINUX_ARM_PREFIX),debug) \
	$(call make-variant,$(LINUX_ARM_PREFIX),release)
LINUX_ENVS := \
	$(call make-variant,$(LINUX_X64_PREFIX),debug) \
	$(call make-variant,$(LINUX_X64_PREFIX),release)

ALL_ENVS := $(WIN_ENVS) $(ARM_ENVS) $(LINUX_ENVS)

# TODO Need to fix variant construction so that
# variant-clone works - possible?

# $(eval $(call build,$variant))
# $variant = The variant to build
define build
  $(call variant-tool,$(1),\
    make-utils/tools/gnu.mk \
    make-utils/tools/variants/base.mk)

  # TODO need higher-order CPPFLAGS (like CPPPATHS)
  $(call variant-append,$(1),CPPFLAGS,-Ilib/utils/include)

  $(eval $(call construct,$(1),lib/build.mk))
  $(eval $(call construct,$(1),app/build.mk))
endef

$(foreach variant,$(ALL_ENVS),$(eval $(call build,$(variant))))

all: $(ALL_TARGETS)
