CC 	:= gcc
CXX 	:= g++
AR 	:= ar
AS 	:= as
OBJDUMP := objdump
LD 	:= ld

ARFLAGS	:= rcs

$(VARIANT)_CC 	:= $($(VARIANT)_TUPLE)-$(CC)
$(VARIANT)_CXX 	:= $($(VARIANT)_TUPLE)-$(CXX)
$(VARIANT)_AR 	:= $($(VARIANT)_TUPLE)-$(AR)
$(VARIANT)_AS 	:= $($(VARIANT)_TUPLE)-$(AS)
$(VARIANT)_OBJDUMP  := $($(VARIANT)_TUPLE)-$(OBJDUMP)
$(VARIANT)_LD 	:= $($(VARIANT)_TUPLE)-$(LD)
