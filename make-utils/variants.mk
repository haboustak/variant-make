# Functions for manipulating variants

# Create a variant from a GCC machine
# VARIANT = $(call make-variant,$toolchain-prefix)
# toolchain-prefix: The arch-specific toolchain prefix to use
# returns the slug defining the new variant
define make-variant
$(strip
  $(eval ARCH_TUPLE := $(call getarch,$(1)gcc))
  $(eval MODE := $(2))
  $(eval VARIANT := $(ARCH_TUPLE)-$(MODE))
  $(eval $(VARIANT)_TUPLE := $(ARCH_TUPLE))
  $(eval $(VARIANT)_DIR := $(BUILD_ROOT) $(ARCH_TUPLE) $(MODE))

  $(eval $(call add-variant,$(call relpath,$($(VARIANT)_DIR))))

  $(VARIANT)
)
endef

# Copy the variables from an existing variant into a new variant
# $(call variant-clone,$source,$copy
# source: The variant with variables to copy
# copy: The variant slug to initialize
# returns the slug of the cloned variant
define variant-clone
$(strip
  $(eval SETTINGS := $(filter $(1)_%,$(.VARIABLES)))
  $(foreach set,$(SETTINGS), \
    $(eval $(subst $(1),$(2),$(set)) := $($(set))))

  $(eval $(2)_TUPLE := $(2))
  $(eval $(2)_DIR := $(BUILD_ROOT) $(2))

  $(eval $(call add-variant,$(call relpath,$($(2)_DIR))))

  $(2)
)
endef

# TODO I'm not sure this belongs here
# Add pattern rules for variants
# $(eval $(call add-variant,$variant-name)
# variant-name: The slug for this variant
define add-variant
$1/%.o: %.c
	@mkdir -p $$(dir $$@)
	@echo "CC $$<"
	$$(strip $(CCOM))
endef

# Append a value to a variant variable (e.g. CFLAGS)
# $(call variant-append,$variant,$variable,$value)
# variant: The variant slug containing the variable
# variable: The variable to append to
# value: The value to append
define variant-append
  $(eval $(1)_$(2) += $(strip $(3)))
endef

# Replace the value of a variant variable
# $(call variant-replace,$variant,$variable,$value)
# variant: The variant slug containing the variable
# variable: The variable to replace
# value: The value to use
define variant-replace
  $(eval $(1)_$(2) := $(strip $(3)))
endef

# Include a tool in a variant
# $(call variant-tool,$variant,$tools)
# variant: The variant slug to receive the tools
# tools: Path to the tools to include
define variant-tool
  $(eval VARIANT := $(1))
  $(eval include $(2))
endef

# Print the variables for a variant
# $(info $(call variant-dump,$variant))
# variant: The variant slug to display
define variant-dump
$(subst $$(nl),,$(subst $$(nl) ,$(nl),$(strip
  $(eval SETTINGS := $(filter $(1)_%,$(.VARIABLES)))
  $(foreach set,$(SETTINGS),\
    $(set) := $($(set))$$(nl))
)))
endef
