CPPFLAGS := -MD -MP
CCOM = $$($$(VARIANT)_CC) \
  $$($$(VARIANT)_CFLAGS) \
  $$($$(VARIANT)_CCFLAGS) \
  $$($$(VARIANT)_CPPFLAGS) \
  -c $$(OUTPUT_OPTION) $$<
LINKCOM = $$($$(VARIANT)_CC) \
  $$($$(VARIANT)_CFLAGS) \
  $$($$(VARIANT)_CPPFLAGS) \
  $$($$(VARIANT)_LDFLAGS) \
  $$^ \
  $$($$(VARIANT)_LDLIBS) \
  -o $$@
ARCOM = $$($$(VARIANT)_AR) \
  $$($$(VARIANT)_ARFLAGS) \
  $$@ \
  $$^

DIR := $(dir $(lastword $(MAKEFILE_LIST)))
include $(DIR)utils.mk
include $(DIR)variants.mk

# $(eval construct,$variant,$makefile)
# $variant = The variant to build for
# $makefile = The makefile to build
define construct
  $(eval VARIANT := $(1))
  $(eval TARGETS := )
  $(eval TARGET_PATHS := )
  $(eval VARIANT_DIR := $($(VARIANT)_DIR))
  $(eval BUILD_DIR := $($(VARIANT)_DIR))
  $(eval $(call build-module,$(2)))
endef

# $(eval $(call build-module,$makefile))
# $makefile = The makefile to build
define build-module
  MODULES :=
  TARGET :=
  SOURCES :=
  LIBS :=
  PREREQS :=

  $(call push,BUILD_DIR,$(dir $(1)))
  DIR := $(call relpath,$(BUILD_DIR))
  BINDIR := $(call relpath,$(VARIANT_DIR) .bin)

  include $(call relpath,$(SRC_DIR) $(1))

  $$(call push,SRC_DIR,$(dir $(1)))

  # start a new target, or continue the old one
  ifneq ($$(strip $$(TARGET)),)
    TGTPATH := $$(DIR)/$$(TARGET)
    $$(TGTPATH)_OBJS :=
    $$(TGTPATH)_DEPS :=
    $$(TGTPATH)_PREREQS :=
    $$(TGTPATH)_LIBS :=
  else
    TARGET := $$(call peek,$$(TARGETS))
    TGTPATH := $$(call peek,$$(TARGET_PATHS))
  endif

  # append the module prereqs to the current target
  $$(TGTPATH)_OBJS += $$(addprefix \
    $$(DIR)/,$$(addsuffix .o,$$(basename $$(SOURCES))))
  $$(TGTPATH)_PREREQS += $$(addprefix $$(BINDIR)/,$$(PREREQS))
  $$(TGTPATH)_LIBS += $$(addprefix -l,$$(LIBS))

  # save the current target, allows children to add targets
  $$(call push,TARGETS,$$(TARGET))
  $$(call push,TARGET_PATHS,$$(TGTPATH))

  # recursively build children in this variant
  ifneq ($$(strip $$(MODULES)),)
    $$(foreach mod,$$(MODULES), \
       $$(eval $$(call build-module,$$(mod))))
  endif

  # restore the current target
  TARGET := $$(call peek,$$(TARGETS))
  TGTPATH := $$(call peek,$$(TARGET_PATHS))
  $$(call pop,TARGETS)
  $$(call pop,TARGET_PATHS)

  # Produce the target rules if the target is ending
  ifneq ($$(TARGET),$$(call peek,$$(TARGETS)))
    INSTALL_TGTPATH := $$(BINDIR)/$$(TARGET)
    ALL_TARGETS += $$(TGTPATH)

    $$(TGTPATH)_DEPS := $$($$(TGTPATH)_OBJS:%.o=%.d)

    $$(eval $$(call add-target,$$(TGTPATH)))
    $$(eval $$(call install-target,$$(TGTPATH),$$(INSTALL_TGTPATH)))
  endif

  $$(call pop,SRC_DIR)
  $$(call pop,BUILD_DIR)
endef

define add-target
  $(eval TGTNAME := $(basename $(notdir $(1))))

  .PHONY: $(TGTNAME)
  $(TGTNAME): $(1)

  ifeq "$$(suffix $(1))" ".a"
    $(1): $($(1)_OBJS)
	@mkdir -p $$(dir $$@)
	@echo "AR $$@"
	$$(strip $(ARCOM))
  else
    $(1): $($(1)_OBJS) $($(1)_PREREQS)
	@mkdir -p $$(dir $$@)
	@echo "LINK $$@"
	$$(strip $(LINKCOM))
  endif

  .PHONY: clean-$(TGTNAME) clean-$(1)
  clean-$(TGTNAME): clean-$(1)
  clean-$(1):
	@echo $(strip CLEAN $(1))
	@$$(strip rm -rf $(1) $($(1)_OBJS))

  .PHONY: clean
  clean: clean-$(1)

  -include $$($(1)_DEPS)

endef

# install the target into the staging dir
# $(eval $(call install-target,$tgt-path,$install-path)
# tgt-path: path to the file to install
# install-path: destination path in the staging dir
define install-target
  $(eval TGTNAME := $(basename $(notdir $(1))))

  $(2): $(1)
	@mkdir -p $$(dir $$@)
	cp $$^ $$@

  .PHONY: clean-$(TGTNAME) clean-$(2)
  clean-$(TGTNAME): clean-$(2)
  clean-$(2):
	@echo $(strip CLEAN $(2))
	@$$(strip rm -rf $(2))

  .PHONY: clean
  clean: clean-$(2)
endef

BUILD_ROOT := build
SRC_DIR :=
VARIANT_DIR :=
ALL_TARGETS :=

.PHONY: all
all:

.PHONY: $(V).SILENT
$(V).SILENT:
