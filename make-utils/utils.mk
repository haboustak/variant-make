# Whitespace helpers
sp :=
sp +=
define nl :=


endef

# Directory Utilities
peek    = $(lastword $1)
push    = $(eval $1 := $($1) $(subst $(sp),*,$2))
pop     = $(eval $1 := $(wordlist 2,$(words $($1)),extra $($1)))
path    = $(abspath $(subst *,$(sp),$(subst $(sp),/,$(strip $1))))
relpath = $(patsubst $(CURDIR)/%,%,$(call path,$1))

getarch   = $(shell $(1) -dumpmachine)
