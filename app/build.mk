# TODO TARGET_TYPE := lib, shlib, prog ?
TARGET := app

# TODO Do I need this distinction?
# SOURCES are built into object using pattern rules
#    $(subst %.c,%.o,$(SOURCES))
#    $(subst %.S,%.o,$(SOURCES))
# PREREQS are just added to the rule
#
# I could just put libraries into SOURCES and then $(filter) to get OBJS
#
SOURCES := main.c config/config.c
PREREQS := libutils.a

# Optionally include sub-makes for organization
MODULES := parsers/build.mk
