#include <stdlib.h>
#include <stdio.h>

#include "main.h"
#include "math.h"
#include "parsers/json.h"

int main(void)
{
    int result = add(4, 3);
    int final = subtract(result, 1);

    const char *data=NULL;
    parse_json(data);

    printf("The result is %d\n", final);
    return 0;
}
